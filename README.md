# Pricing Table Web App

This is a simple web application for creating a pricing table using Bootstrap. It allows users to select the number of users they need and see pricing options that match their requirements. The project includes responsive design, form handling for user inquiries, and lazy-loading images to improve performance.

## Features

- Responsive pricing table with multiple pricing tiers.
- Dynamic pricing based on the selected number of users.
- User inquiry forms for each pricing tier.
- Lazy-loading images for improved performance.
- Navigation bar with links to different sections.
- Sign-up button to encourage user engagement.

## How to Use

1. Clone this repository to your local machine.
2. Open the `index.html` file in your web browser.

## Project Structure

- `index.html`: The main HTML file containing the web page structure.
- `pricing.css`: Custom CSS styles for the pricing table and other elements.
- `images/`: Directory containing the images used in the pricing table.
- `scripts/`: Directory containing JavaScript files.
  - `lazy-load.js`: JavaScript code for lazy-loading images.
  - `form.js`: JavaScript code for opening user inquiry forms.
  - `slider.js`: JavaScript code for updating pricing based on user input.

## Dependencies

- [Bootstrap](https://getbootstrap.com/): Used for styling and layout.
- [jQuery](https://jquery.com/): JavaScript library for DOM manipulation.
- [Intersection Observer API](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API): Used for lazy-loading images when they come into view.
- [Holder.js](http://holderjs.com/): Used for generating placeholder images.

## Usage

- Customize the pricing tiers, features, and details in the HTML and CSS to fit your product or service.
- Modify the user inquiry forms to connect to your backend or service for handling inquiries.
- Update the navigation links, company name, and footer information to match your branding.
- Enhance and extend the functionality as needed for your specific use case.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Author

Naman Khatwani

## Acknowledgments

- This project was created using [Bootstrap](https://getbootstrap.com/).
- Lazy-loading images implemented with the Intersection Observer API.
- Form handling code for opening forms in new windows.
- Slider for selecting the number of users and updating pricing.

Feel free to contribute, report issues, or suggest improvements to this project. Enjoy using and customizing this simple pricing table web app!
